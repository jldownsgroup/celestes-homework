
def restore_from_files()
  output = []
  # choose backup folder
  backup_root_path = "backups/"
  # just choose the first one right now.
  backup_folder = File.join(backup_root_path, "words")
  word_folders = Dir[File.join(backup_folder, "/*")]
  word_folders.each do |folder|
    name = File.basename folder
    sound = Dir[File.join(folder, "/*.m4a"),
                File.join(folder, "/*.mp3")
               ].first
    video = Dir[File.join(folder, "/*.mp4")].first
    output = output + [name, sound, video]
    if not Word.find_by_name(name)
      puts "trying to restore #{name}"
      w = Word.new(name: name)
      w.sound.attach(io: File.open(sound), filename: File.basename(sound))
      w.video.attach(io: File.open(video), filename: File.basename(video)) if video
      w.save
      sleep (0.2).second
    end
  end

  return output
end
