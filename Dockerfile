FROM ruby:2.7

# Install node and yarn
RUN curl https://deb.nodesource.com/setup_12.x | bash
RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update && apt-get install -y nodejs yarn git less nano ffmpeg

COPY . ./code

RUN cd /code && bundle install && \
	yarn

RUN cd /code && rake app:update:bin && yes | rails webpacker:install && \
	rake app:update:bin

COPY ./config/webpacker.yml /code/config/webpacker.yml

RUN cd /code && \
    rails db:migrate RAILS_ENV=production SECRET_KEY_BASE=$(rake secret)

CMD cd /code && rake app:update:bin && rails s -b 0.0.0.0 -e production

