Rails.application.routes.draw do
  resources :words
  root 'single_page_apps#type_words'
  get  '/math', to: 'single_page_apps#math_quiz'
  get  '/backup', to: 'words#backup'
  get  '/restore', to: 'words#restore'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
