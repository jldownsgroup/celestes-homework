class Word < ApplicationRecord
  has_one_attached :sound
  has_one_attached :video

  validates :name, presence: true
  validates :sound, presence: true, blob: { content_type: :audio }
  validates :video, blob: { content_type: :video }
end
