require 'streamio-ffmpeg'
require 'restore_from_files'

module WordsHelper

  # if the attached video is a .mov, convet it to mp4
  def ensure_video_is_mp4(word_object)
    if word_object.video.content_type != "video/mp4"
      word_object.video.open do |v|
        FFMPEG::Movie.new(v.path).transcode("tmp/transcoded.mp4")
        word_object.video.attach(
          io: File.open("tmp/transcoded.mp4"),
          filename: "#{word_object.name}.mp4")
      end
    end
  end

end

class WordsController < ApplicationController
  before_action :set_word, only: %i[ show edit update destroy ]

  # GET /words or /words.json
  def index
    @words = Word.all
  end

  # GET /words/1 or /words/1.json
  def show
  end

  # GET /words/new
  def new
    @word = Word.new
  end

  # GET /words/1/edit
  def edit
  end

  # POST /words or /words.json
  def create
    # convert movies to the correct format
    @word = Word.new(word_params)

    respond_to do |format|
      if @word.save
        helpers.ensure_video_is_mp4(@word)
        format.html { redirect_to @word, notice: "Word was successfully created." }
        format.json { render :show, status: :created, location: @word }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /words/1 or /words/1.json
  def update
    respond_to do |format|
      if @word.update(word_params)
        helpers.ensure_video_is_mp4(@word)
        format.html { redirect_to @word, notice: "Word was successfully updated." }
        format.json { render :show, status: :ok, location: @word }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /words/1 or /words/1.json
  def destroy
    @word.destroy
    respond_to do |format|
      format.html { redirect_to words_url, notice: "Word was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  # BACKUP ;) Save the records to disk in a human-readable way
  def backup

    # create backup path
    backup_root_path = File.join("backups/", "words")
    FileUtils.mkdir_p(backup_root_path)

    Word.all.each do |word|
      # create a folder with the word as its name
      word_folder = File.join(backup_root_path, word[:name])
      FileUtils.mkdir_p(word_folder)

      # and save the video and audio associated with it
      word.video.open do |f|
        new_video_filename = word.name + File.extname(f.path)
        FileUtils.cp(f.path, File.join(word_folder, new_video_filename))
      end
      word.sound.open do |f|
        new_sound_filename = word.name + File.extname(f.path)
        FileUtils.cp(f.path, File.join(word_folder, new_sound_filename))
      end # word.sound.open
    end # Word.all.each

    @backup_folder = File.join(backup_root_path, "**/*")
    @backup_contents = Dir.glob(@backup_folder)
  end

  def restore
    @output = restore_from_files()
    # render results
    @words = Word.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_word
      @word = Word.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def word_params
      params.require(:word).permit(:name, :sound, :video)
    end
end
