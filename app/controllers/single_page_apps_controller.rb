class SinglePageAppsController < ApplicationController
  def type_words
    @words = Word.all

    music_tracks = Dir["public/music_tracks/*.mp3"].map do |filename|

      {
        "path" => "/music_tracks/#{File.basename filename}",
        "name" => File.basename(filename)
      }
    end

    @server_data = {
      "music_tracks" => music_tracks
    }
  end

  def math_quiz
    word_sounds = Dir["public/math_sounds/*.mp3",
                      "public/math_sounds/*.m4a",
                      "public/math_sounds/*.wav",].map{ |filename|
      extension = File.extname(filename)
      word = File.basename(filename, extension) # remove extension
      {
        "word" => word,
        # ../ notation for assets in the pipeline, like sound files
        "sound_file" => "../math_sounds/#{File.basename filename}"
      }
    }


    goodjob_videos = Dir["public/math_sounds/*.mp4"].map{ |filename|
        # / notation for assets in the public folder, like videos
        "/math_sounds/#{File.basename filename}"
    }

    @server_data = {
      "word_sounds" => word_sounds,
      "goodjob_videos" => goodjob_videos
    }

  end
end
