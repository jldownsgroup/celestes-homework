module SinglePageAppsHelper

  def random_math_problem
    first_number = rand(1..10)
    second_number = rand(1..10)
    operator = ["+", "-"].sample
    if (operator == "-") and (first_number < second_number)
      foo = first_number
      first_number = second_number
      second_number = foo
    end

    return "#{first_number} #{operator} #{second_number} ="
  end

end

