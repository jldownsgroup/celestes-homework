import { Controller } from 'stimulus';

var soundFile = "";

// ** Stimulus Controls
export default class extends Controller {

	// static targets = ["wordArea"]
	static values = { soundpath: String }

	connect() {
		console.log("showword controller reporting");;
		soundFile = new Howl({src: [this.soundpathValue]})
	}

	playword() {
		soundFile.play()
	}
}
