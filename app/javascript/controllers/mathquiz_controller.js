// mathquiz_controller.js
//
// Stimulus controller for the math quiz part

import { Controller } from 'stimulus';
import {Howl, Howler} from 'howler';

// ** Global variables
var math_problem_intro_voice=1;
const DIGITS = "1234567890".split("");
const LETTERS = "abcdefghijklmnopqrstuvwxyz".split("");

// ** Stimulus Controls
export default class extends Controller {
    static targets = ["mathProblemSpan", "mathAnswerSpan", "videoPlayer"]
	  static values = { serverdata: String }
// *** helpers
    play_letter_sound(letter) {
	      if (letter in this.sound_objects) {
		        this.sound_objects[letter].play();
	      } else {
		        this.sound_objects["tick"].play();
	      }
    }

    get_letter_sound(letter) {
	      if (letter in this.sound_objects) {
		        return this.sound_objects[letter];
	      } else {
		        return this.sound_objects["tick"];
	      }
    }

    play_howl_chain(word_array) {
        if (word_array.length > 1) {
            word_array[0].once('end', () => {
                this.play_howl_chain(word_array.slice(1))
            });
        };

        word_array[0].play();
    }

    // play_howl_chain(word_array) {
    //     if (word_array.length == 1) {
    //         word_array.pop().play();
    //     } else {
    //         console.log(word_array)
    //         var last_sound = word_array.pop();
    //         var penultimate_sound = word_array[word_array.length - 1]
    //         penultimate_sound.once('end', ()=> last_sound.play());
    //         this.play_howl_chain(word_array);
    //     }
    // }

    say_math_problem() {
        var math_problem = this.mathProblemSpanTarget.innerHTML;
        math_problem = math_problem
            .replace("-", "minus")
            .replace("+", "plus")
            .replace("=", "equals");
        this.play_howl_chain(math_problem.split(" ").map(x => this.sound_objects[x]));
    }

// *** initialize
    initialize() {
    }

// *** connect
    connect() {
        // Retreive server data drop
		    this.server_data = JSON.parse(this.serverdataValue);
        console.log(JSON.stringify(this.server_data));

        // load sounds
        this.sound_objects = {}
        this.server_data["word_sounds"].forEach(h => {
            this.sound_objects[h["word"]] = new Howl({src: h["sound_file"]});
        });
    }

// *** register key
	// This function handles key presses
	  registerKey(event_object) {
		    console.log(event_object);

        var problem = this.mathProblemSpanTarget.innerHTML.split(" ");
        var first_number = parseInt(problem[0]);
        var second_number = parseInt(problem[2]);
        var operation = problem[1];
        console.log(operation)
        var answer = 0;
        if (operation == "+") {answer = first_number + second_number;};
        if (operation == "-") {answer = first_number - second_number;};

		    var key = event_object.key.toUpperCase();

        if (key in DIGITS) {
            if (this.mathAnswerSpanTarget.innerHTML.length < 2) {
                this.mathAnswerSpanTarget.innerHTML += key;
                this.play_letter_sound(this.mathAnswerSpanTarget.innerHTML);
            }
        }
        // say the math problem
        else if (event_object.code == "Space") {
            this.say_math_problem();
        } else if (event_object.code == "Backspace") {
            if (this.mathAnswerSpanTarget.innerHTML.length >= 1) {
                this.mathAnswerSpanTarget.innerHTML = this.mathAnswerSpanTarget.innerHTML.slice(0, -1);
            }
        } else if (event_object.code == "Enter") {
            console.log(answer)
            if (this.mathAnswerSpanTarget.innerHTML == answer) {
                // play video
			          this.videoPlayerTarget.src = this.server_data["goodjob_videos"][0]
			          this.videoPlayerTarget.play();
                // wait and new problem
			          setTimeout(() => {Turbolinks.visit(window.location.href);}, 2500);
            } else {
                // play "try again"
		            this.sound_objects["tryagain"].play();
                this.mathAnswerSpanTarget.innerHTML = "";
            }

        } else {
		        this.play_letter_sound(key.toLowerCase());
        }

	}

    gohome() {
        Turbolinks.visit("/");
    }
}
