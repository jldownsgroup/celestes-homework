import { Controller } from 'stimulus';
import {Howl, Howler} from 'howler';
const micromatch = require('micromatch');

// ** Load "letter sounds"
var letter_sound_data = {
	tick: new Howl({src: ["../letter_sounds/sound_click_tick.wav"]})
};

const letters = "abcdefghijklmnopqrstuvwxyz1234567890";
Array.from(letters)
	.forEach(l => {
		letter_sound_data[l] = new Howl({src: ["../letter_sounds/"+l+".mp3"]})
	});

function play_letter_sound(letter) {
	if (letter in letter_sound_data) {
		letter_sound_data[letter].play();
	} else {
		letter_sound_data["tick"].play();
	}
}


// ** Stimulus Controls
export default class extends Controller {

	static targets = ["wordArea", "videoPlayer", "hint", "hintArea", "hintCarousel"]
	  static values = { worddata: String, serverdata: String }

// *** helpers
    play_music() {
        console.log("playing music");
        var music_path = this.server_data["music_tracks"][0]["path"];
        console.log("path:", music_path)
        var music_howl = new Howl({src: [music_path]});
        music_howl.play();
    }
// *** initialize
	initialize() {
		  this.hint_index = 1;
		  this.showCurrentHint();
	}

// *** connect
	connect() {

      // Retreive server data drop
      var decoded_serverdata = decodeURIComponent(this.serverdataValue);
		  this.server_data = JSON.parse(decoded_serverdata);

		  console.log("eeedf from StimulusJS");;
		  this.word_data = JSON.parse(this.worddataValue)
		  this.word_list = Object.keys(this.word_data);
		  this.word_list.forEach(w=> {
			    this.word_data[w].sound = new Howl({src: [this.word_data[w].sound_url]});
		  });
		  console.log(this.word_list);;
		  // hide hints at first
		  this.hintCarouselTarget.hidden = true;
	}

// *** register key
	// This function handles key presses
	registerKey(event_object) {

		console.log(event_object);

		// play letter sound
		var key = event_object.key.toUpperCase();
		play_letter_sound(key.toLowerCase());

		// handle special keys
		switch(key) {
		case "ARROWLEFT":
			this.hint_index--;
			if (this.hint_index < 0) {
				this.hint_index = this.hintTargets.length-1;
			}
			this.showCurrentHint();
			break;
		case "ARROWRIGHT":
			this.hint_index++;
			if (this.hint_index > this.hintTargets.length-1) {
				this.hint_index = 0;
			}
			this.showCurrentHint();
			break;
		}

		// see if word is a match
		var current_word = this.wordAreaTarget.innerHTML + key;
		var matching_word_list = micromatch(this.word_list, current_word+"*");

		// no matching word
		if (matching_word_list.length == 0) {
			if (letters.toUpperCase().includes(key)) {
				current_word = key;
			} else {
				current_word = "";
			}
			// hide video
			  this.videoPlayerTarget.src = "";
			// show hints
			this.hintAreaTarget.hidden = false;
		}

		// perfect match
		if (micromatch.isMatch(current_word, matching_word_list)) {
			  console.log("perfect matct!", current_word);
			  // play sound
			  setTimeout(() => {  this.word_data[current_word].sound.play(); }, 500);
			  // show video
			  this.videoPlayerTarget.src = this.word_data[current_word].video_url;
			  this.videoPlayerTarget.play();
			  // hide hints
			  this.hintAreaTarget.hidden = true;
        // handle special words
        console.log("in the elseif");
        if (current_word == "MATH") {
			      setTimeout(() => {this.wordAreaTarget.innerHTML = ""; Turbolinks.visit("/math");}, 7000);
        } else if (current_word == "MUSIC") {
            this.play_music();
        }
		}

		this.wordAreaTarget.innerHTML = current_word;
	}

// *** show current hint
	// Show all but one hint.
	showCurrentHint() {
		this.hintTargets.forEach((element, index) => {
			element.hidden = index != this.hint_index;
		})
	}

	toggleHints() {
		this.hintCarouselTarget.hidden = !this.hintCarouselTarget.hidden;
	}
}
