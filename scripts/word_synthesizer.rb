# Word Synthesizer
require 'fileutils'

backup_directory = File.join("../backups", Time.now.strftime("%y%m%d_%k%m%S"))
words = (10..99).to_a.map{ |x| x.to_s }

words.each do |word|
  # make directory
  word_directory = File.join(backup_directory, word)
  FileUtils.mkdir_p(word_directory)

  # save sound
  sound_filename = File.join(word_directory, "#{word}.mp3")
  puts `curl "http://www.google.com/speech-api/v1/synthesize?lang=en-us&text=#{word}" -o #{sound_filename}`
end
