# Celeste's Homework

Celeste's Homework is a self-hosted learning platform for young children. It is currently aimed at preschool- and kindergarten-aged kids who are curious about computers and have been introduced to letters and numbers. Its primary purpose is to teach children the basics of using a keyboard and mouse while also improving spelling and simple math along the way.

## Documentation

Instructions on how to install and configure this platform can be found at https://celestes-homework.justindowns.com/

## Demo

You can play with an online demo here: https://c-demo.jldowns.com/
